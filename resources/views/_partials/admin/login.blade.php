@extends('_app.admin')

@section('content')
    <div class="login-logo">
        <a><b>IT</b>INDO</a>
    </div>
    <div class="login-box-body">
        <form action="{{ route('admin.login.process') }}" method="post">
            @csrf
            <div class="form-group has-feedback">
                <input type="text" name="username" class="form-control" placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                </div>
            </div>
            <div class="col-xs-4">
                <br>
                <br>
                <br>
                <br>
                <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div>
        </form>
    </div>
@endsection