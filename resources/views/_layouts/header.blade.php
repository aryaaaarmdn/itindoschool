<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>IT Indo School</title>
    
    @if(Route::is('admin.*'))
        @include('_resources.admin.headResource')
    @else
        @include('_resources.homepage.headResource')
    @endif
    <script data-ad-client="ca-pub-2508111236087801" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>

@if(Route::is('admin.*'))
    @if(Route::is('admin.login'))
        <body class="hold-transition login-page">
    @else
        <body class="hold-transition skin-blue sidebar-mini">
    @endif
@else
    <body id="page-top">
@endif



