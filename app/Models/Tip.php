<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tip extends Model
{
    use HasFactory;

    protected $fillable = [
        'material_id',
        'judul',
        'gambar',
        'artikel'
    ];

    public function material()
    {
        return $this->belongsTo(Material::class);
    }

}
