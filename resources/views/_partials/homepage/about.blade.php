@extends('_app.homepage', ['isAboutActive' => true])

@section('title', 'About')

@section('content')
<div class="container">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#">Home</a>
        </li>
        <li class="breadcrumb-item active">About</li>
    </ol>
    <div class="row">
        <div class="col-lg-6">
		    <div class="video-materi">
                <video controls autoplay>
                <source src="{{ asset('assets/video/intro.mp4') }}" type="video/mp4">
                Your browser does not support the video tag.
                </video>
		    </div>
        </div> 
		
        <div class="col-lg-6"><br>
            <h2>ABOUT IT INDO SCHOOL</h2>
            <p>ITINDOSCHOOL adalah sebuah website yang mempunyai visi untuk mendukung pendidikan indonesia di bidang Teknologi </p>
            <p>Berawal dari kepedulian kami terhadap pendidikan teknologi di indonesia yang sudah tertinggal. </p><br><br><br>
            <p>Bergabung bersama kami menjadi relawan membangun negeri lebih maju dan terpelajar.</p>
            <br>
            <p>E-mail : mail@itindoschool.com</p>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <br><br>
                <h2>JOIN US</h2>
                <p>Ingin menjadi Pengajar atau berkontribusi di ITINDOSCHOOL selengkapnya di bawah ini</p>
                <br><br>
                <h5>Pengajar</h5>
                <p class="justify">menjadi pengajar membuat video tutor , materi , dan soal mendapatkan nilai tambah diri kalian dan mendapatkan uang saku untuk terus berkontribusi bersama kami membangun negeri</p>
                <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Bergabung</button>
                <div id="demo" class="collapse">
                
                    <div class="jumbotron">
                        <h2>DAFTAR </h2>
                        <p>silahkan masukan data diri kalian, pastikan diri kalian konsisten bergabung bersama kami dan mengikuti peraturan untuk pembuatan konten dari kami</p>
                        <form action="#" class="needs-validation" novalidate>
                          
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Your Email" required>
                                <div class="input-group-append">
                                    <span class="input-group-text">@itindoschool.com</span>
                                </div>
                                <div class="valid-feedback">Valid.</div>
                                <div class="invalid-feedback">Please fill out this field.</div>
                            </div>
                          
                            <div class="form-group">
                                <label for="pwd">Password:</label>
                                <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pswd" required>
                                <div class="valid-feedback">Valid.</div>
                                <div class="invalid-feedback">Please fill out this field.</div>
                            </div>
                            <div class="form-group form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="remember" required>I agree.
                                    <div class="valid-feedback">Valid.</div>
                                    <div class="invalid-feedback">Check this checkbox to continue.</div>
                                </label>
                            </div>
                            <button type="submit" class="btn btn-primary">DAFTAR</button>
                        </form>
                        <br>
                        <a href="#"><button type="button" class="btn btn-primary" >LOGIN</button></a>
                    </div>
                    <script>
                    // Disable form submissions if there are invalid fields
                        (function() {
                        'use strict';
                        window.addEventListener('load', function() {
                            // Get the forms we want to add validation styles to
                            var forms = document.getElementsByClassName('needs-validation');
                            // Loop over them and prevent submission
                            var validation = Array.prototype.filter.call(forms, function(form) {
                            form.addEventListener('submit', function(event) {
                                if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                                }
                                form.classList.add('was-validated');
                            }, false);
                            });
                        }, false);
                        })();
                    </script>

                </div>
                <br><br>
                <h5>Berkontribusi</h5>
                <p class="justify">ikut membantu memberikan donasi untuk para pengajar dan untuk mengembangkan situs ini agar tercapai semua visi dan misi kami untuk membangun negeri</p>
                <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo1">Bergabung</button>
                <div id="demo1" class="collapse">
                    <div class="jumbotron">
                        <p>
                            Kalian bisa berdonasi dalam bentuk dan jumlah yang tidak ditentukan hubungi nomor WA berikut 085719719528 
                        </p>
                        terimakasih untuk donasi yang kalian berikan untuk membangun negeri 
                    </div>
              </div>
            </div>              
            <div class="col-lg-6">
                <br><br><br><br>
                <img src="{{ asset('assets/img/about2.png') }}" class="rounded" alt="Cinque Terre" width="500" height="350"> 
            </div> 
        </div>       
    </div>
</div>
@endsection