<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Material;
use App\Models\SubMaterial;
use App\Models\DataMaterial;
use App\Models\Tip;


class Homepage extends Controller
{
    public function index()
    {
        $materi = Material::all();

        return view('_partials.homepage.main', ['materi' => $materi]);
    }

    public function about()
    {
        return view('_partials.homepage.about');
    }

    public function forum()
    {
        return view('_partials.homepage.forum');
    }

    public function materi($materi)
    {
        $materi = Material::where('nama', $materi)->firstOrFail();
        $submateri = $materi->subMaterials;
        return view('_partials.homepage.materi', ['submateri' => $submateri, 'materi'  => $materi]);
    }

    public function detailMateri($materi, $detailMateri, $index = 1) 
    {
        $materiModel = Material::firstWhere('nama', $materi);
        $subMateriModel = SubMaterial::firstWhere('nama', $detailMateri);

        $totalMateri = $subMateriModel->dataMaterials;
        $dataMateri = DataMaterial::where('sub_material_id', $subMateriModel->id)
                        ->where('no_materi', $index)->first();

        $data = [
            'breadcrumbs1' => $materi,
            'breadcrumbs2' => $detailMateri,
            'total_materi' => $totalMateri,
            'dataMateri' => $dataMateri
        ];

        return view('_partials.homepage.bacamateri', $data);
    }

    public function Tips($id_materi = null)
    {
        // $data = Material::find($id_materi)->tips ?? Tip::all();
        // $data = Material::find($id_materi)->tips()->paginate(4) ?? Tip::paginate(4);
        $data = $id_materi ? Material::find($id_materi)->tips()->paginate(4) : Tip::paginate(4);
        $materi = Material::all();
        $tips = Tip::all();

        return view('_partials.homepage.tips', ['data' => $data, 'materi' => $materi, 'id_materi' => $id_materi, 'tips' => $tips]);
    }

    public function BacaTips($id_tips)
    {
        $tips = Tip::findOrFail($id_tips);

        return view('_partials.homepage.bacatips', ['data' => $tips]);
    }

}
