<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function profile()
    {
        return view('_partials.homepage.profile');
    }

    public function editprofile()
    {
        return view('_partials.homepage.edit_profile');
    }

    public function updateProfile(Request $request)
    {
        // Identifikasi user 
        $user = Auth::user();

        // Validasi Foto
        $request->validate([
            'foto' => 'file|mimes:jpg,jpeg,png'
        ]);

        $data = [
            'nama' => $request->filled('nama') ? 
                        $request->nama : $user->nama,
            'email' => $request->filled('email') ? 
                            $request->email : $user->email,
            'password' => $request->filled('password') ? 
                            Hash::make($request->password) : $user->password,
            'no_telp' => $request->filled('no_telp') ?
                            $request->no_telp : $user->no_telp,
            'img' => $request->hasFile('foto') ? 
                        $request->file('foto')->hashName() : $user->img,
        ];


        // Proses Logic Foto
        if ( $request->hasFile('foto') ) { // Jika ada foto upload baru
            if ( $user->img === null ) { // Jika foto sebelumnya == null  
                // Upload foto;
                $request->file('foto')
                        ->move(public_path('upload/ava/'), $request->file('foto')->hashName());
                
            } else { // Jika foto sebelumnya tidak null
                // Hapus foto lama
                unlink(public_path('upload/ava/') . $user->img);
                // Upload Foto baru
                $request->file('foto')
                        ->move(public_path('upload/ava/'), $request->file('foto')->hashName());
            }
        }



        $update = $user->update($data);

        if ( $update ) {
            return redirect()->back()->with('msg', ['success', 'Data Berhasil Diperbarui']);
        } else {
            return redirect()->back()->with('msg', ['danger', 'Data Gagal Diperbarui']);
        }

    }
}
