@extends('_app.admin', ['isSubMateriActive' => true])

@section('content-header', 'Data Sub Materi')

@section('breadcrumb')
    <li class="active">
        Data Sub Materi
    </li>
@endsection

@section('content')
    <div class="col-lg-12">
	    <div class="box">
           <div class="box-header">
               <h3 class="box-title">Data Sub Materi</h3>
           </div>
           <div class="box-body">
                <a href="{{ route('admin.add.submateri') }}" class="btn btn-primary btn-md">
                    Tambah Sub Materi
                </a>
                <br>
                <br>
                @if( session('status') )
                    <div class="alert alert-{{ session('alert') }}">
                        {{ session('status') }}
                    </div>
                @endif
                <table class="table table-stripped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Sub Materi</th>
                            <th>Nama Jenis Materi</th>
                            <th>Img Thumbnail</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $key = 1;
                        @endphp
                        @foreach($data as $submateri)
                            <tr>
                                <td>{{ $key }}</td>
                                <td>{{ $submateri->nama }}</td>
                                <td>{{ $submateri->material->nama }}</td>
                                <td>
                                    <img width="200" 
                                        src="{{ asset('upload/submateri') . '/' . $submateri->img_thumb }}" alt="Submateri image">
                                </td>
                                <td>
                                    <a class="btn btn-flat btn-primary" 
                                        href="{{ route('admin.submateri.soalujian', $submateri->id) }}">
                                        Soal Ujian
                                    </a>
                                    
                                    <a class="btn btn-flat btn-primary" 
                                        href="{{ route('admin.edit.submateri', $submateri->id) }}">
                                        Edit
                                    </a>
                                    <a class="btn btn-flat btn-danger btnDelete deleteSubMateriButton"  
                                        data-target="#deleteData" data-toggle="modal"  
                                        data-id="{{ $submateri->id }}">
                                        Delete
                                    </a>
                                </td>
                            </tr>
                            @php
                                $key++;
                            @endphp
                        @endforeach
                    </tbody>
                </table>

                <ul class="pagination justify-content-center" style="margin:10px 40%;">
                    <li class="page-item">
                        <a class="page-link" href="#">Previous</a>
                    </li>
                            
                    <li class="page-item">
                        <a class="page-link" href="#"></a>
                    </li>

                    <li class="page-item">
                        <a class="page-link" href="#">Next</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection
