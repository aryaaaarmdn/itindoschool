@extends('_app.admin', ['isUsersActive' => true])

@section('content-header', 'Data Users')

@section('breadcrumb')
    <li class="active">
        Data Users
    </li>
@endsection

@section('content')

    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Data User</h3>
            </div>
            <div class="box-body">
                @if( session('status') )
                    <div class="alert alert-{{ session('alert') }}">
                        {{ session('status') }}
                    </div>
                @endif
                <table class="table table-stripped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No.id</th>
                            <th>Nama </th>
                            <th>Email</th>
                            <th>Aksi</th>
                            <th>img</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $no = 1;
                        @endphp
                        @foreach($data as $user)
                            <tr>
                                <td>{{ $no }}</td>
                                <td>{{ $user->nama }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    <a class="btn btn-flat btn-primary" 
                                        href="{{ route('admin.edit.user', $user->id) }}">
                                        Edit
                                    </a>
                                    <a class="btn btn-flat btn-danger btnDelete deleteUserButton" 
                                        data-toggle="modal" 
                                        data-id="{{ $user->id }}" 
                                        data-target="#deleteData">
                                        Delete
                                    </a>
                                </td>
                                <td>{{ $user->img }}</td>
                            </tr>
                            @php
                                $no++;
                            @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection