<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    use HasFactory;

    protected $fillable = ['nama'];

    public function subMaterials()
    {
        return $this->hasMany(SubMaterial::class);
    }

    public function tips()
    {
        return $this->hasMany(Tip::class);
    }
}
