@extends('_app.admin', ['isMateriActive' => true])

@section('content-header', 'Data Materi')

@section('breadcrumb')
    <li class="active">
        Data Materi
    </li>
@endsection

@section('content')

    <div class="col-lg-12">
	    <div class="box">
           <div class="box-header">
               <h3 class="box-title">Data Jenis Materi</h3>
           </div>
           <div class="box-body">
                <a href="{{ route('admin.add.materi') }}" class="btn btn-primary btn-md">Tambah Jenis Materi</a>
                <br>
                <br>
                @if( session('status') )
                    <div class="alert alert-{{ session('alert') }}">
                        {{ session('status') }}
                    </div>
                @endif
                <table class="table table-stripped table-bordered table-hover">
	                <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Jenis Materi</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $key = 1;
                        @endphp
                        @foreach($materi as $materi)
                            <tr>
                                <td>
                                    {{ $key }}
                                </td>
                                <td>
                                    {{ $materi['nama'] }}
                                </td>
                                <td>
                                    <a class="btn btn-flat btn-primary" 
                                        href="{{ route('admin.edit.materi', $materi['id']) }}">
                                        Edit
                                    </a>
                                    <a class="btn btn-flat btn-danger btnDelete deleteMateriButton"
                                        data-toggle="modal" data-target="#deleteData" 
                                        data-id="{{ $materi->id }}"
                                    >
                                        Delete
                                    </a>
                                </td>
                            </tr>
                            @php
                                $key++;
                            @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection