<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\Admin;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Admin;
        $admin->nama = "Admin";
        $admin->email = "admin@mail.com";
        $admin->username = "Admin";
        $admin->password = Hash::make('123'); 
        $admin->save();
    }
}
