@include('_layouts.header')

@php

    use App\Models\Material;
    
    $materi = Material::all();

@endphp

<nav class="navbar navbar-b navbar-trans navbar-expand-md fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll" href="#page-top">
            <img src="{{ asset('assets/img/itindo.png') }}" style="height: 37px;width: 37px;"> IT INDO SCHOOL
        </a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
        aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link js-scroll {{ isset($isHomeActive) ? 'active' : '' }}" href="{{ route('homepage') }}">Home</a>
                </li>
                <li class="nav-item dropdown {{ isset($isMateriActive) ? 'active' : '' }}">
                    <a href="#" class=" nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Materi</a>
                    <ul class="dropdown-menu">
                        @foreach($materi as $materi)
                            <li class="nav-link {{ isset($isMateriActive) ? 'active' : '' }}">
                                @auth
                                    <a href="{{ route('materi', $materi->nama) }}">
                                        {{ $materi->nama }}
                                    </a>
                                @endauth
                                @guest
                                    <a href="#" data-toggle="modal" data-target="#modalmasuk">
                                        {{ $materi->nama }}
                                    </a>
                                @endguest
                            </li>
                        @endforeach
                    </ul>
                </li>
                <li class="nav-item {{ isset($isTipsActive) ? 'active' : '' }}">
                    <a class="nav-link js-scroll" href="{{ route('tips') }}">Tips </a>
                </li>
                <li class="nav-item {{ isset($isForumActive) ? 'active' : '' }}">
                    <a class="nav-link js-scroll" href="{{ route('forum') }}">Forum</a>
                </li>
                <li class="nav-item {{ isset($isAboutActive) ? 'active' : '' }}">
                    <a class="nav-link js-scroll" href="{{ route('about') }}">About</a>
                </li>
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#modalmasuk"><i class="fa fa-user" aria-hidden="true"></i> Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#modaldaftar"><i class="fa fa-sign-in" aria-hidden="true"></i> Register</a>
                    </li>
                @endguest
                @auth
                    <li class="nav-item {{ isset($isProfileActive) ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('profile') }}">
                            <i class="fa fa-user" aria-hidden="true"></i> 
                            Profil
                        </a>
                    </li>
                    <li class="nav-item">
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf
                            <a class="nav-link" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                        this.closest('form').submit();">
                                <i class="fa fa-sign-out" aria-hidden="true"></i> 
                                Logout
                            </a>
                        </form>
                    </li>
                @endauth
            </ul>
        </div>
    </div>
</nav>

  <!-- <-- Login -->
<div class="modal fade" id="modalmasuk">
    <div class="modal-dialog">
        <div class="modal-content">
      
            <!-- Modal Header -->
            <div class="modal-header ">
                <h4 class="modal-title">Login IT INDO SCHOOL</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
        
            <!-- Modal body -->
            <div class="modal-body">
                <form action="{{ route('login') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1"><h6>Username</h6></label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-user"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="Masukkan E-mail" name="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><h6>Password</h6></label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-lock"></i></span>
                            </div>
                            <input type="password" class="form-control" placeholder="Password..." name="password">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
              <table width="100%">
                <tr>
                    <td width="40%"></td>
                    <td width="10%"></td>
                    <td width="50%">
                    <center>
                        <button type="submit" class="btn btn-success text-center">
                                    Login Now
                        </button>
                        <button type="button" class="btn btn-danger text-center" data-dismiss="modal">Close</button>
                    </center>
                    </td>
                </tr>
                <tr>
                  <td width="40%"></td>
                  <td width="10%"></td>
                  <td width="50%">
                    <center>   
                      <a href="#" >
                        <img src="{{ asset('assets/images/btn-google.png') }}" width="90%">
                      </a>
                    </center> 
                  </td>
                </tr>
              </table>
            </div>
                </form>        
        </div>
    </div>
</div>

<!-- Register -->
<div class="modal fade" id="modaldaftar">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header ">
                <h4 class="modal-title">Daftar IT INDO SCHOOL</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
        
            <!-- Modal body -->
            <div class="modal-body">
                <form action="{{ route('register') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1"><h6>Username</h6></label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-user"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="Username" name="nama">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><h6>E-mail</h6></label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">@</span>
                            </div>
                            <input type="email" class="form-control" placeholder="example@mail.com" name="email" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"><h6>Password</h6></label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-lock"></i></span>
                            </div>
                            <input type="password" class="form-control" placeholder="Password...." name="password">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary text-center">Login Now</button>
                <button type="button" class="btn btn-danger text-center" data-dismiss="modal">Close</button>
            </div>
                </form>        
        </div>
    </div>
</div>

    <!-- Homepage -->
    @if(Route::currentRouteName() == 'homepage')
        <div id="home" class="intro route bg-image" style="background-image: url({{ asset('assets/img/post-3.jpg') }})">
            <div class="overlay-itro"></div>
            <div class="intro-content display-table">
                <div class="table-cell">
                    <div class="container">
                        <h1 class="intro-title mb-4"> IT INDO SCHOOL</h1>
                        <p class="intro-subtitle">Tutorial <span class="text-slider-items">Programing,Robotic,Server,Jaringan,Design</span><strong class="text-slider"></strong></p>
                    </div>
                </div>
            </div>
        </div>
    @elseif(Route::is('baca.tips'))
        <div id="about" 
            class="intro route bg-image" 
            style="background-image: url({{ asset('upload/tips') . '/' . $gambar }});height: 600px;">
            <div class="overlay-itro"></div>
            <div class="intro-content display-table">
                <div class="table-cell">
                    <div class="container">
                        <h1 class="intro-title mb-4" style="padding-top: 100px;">
                            @yield('title')
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    @else
    <!-- !Homepage -->
        <div id="about" class="intro route bg-image" style="background-image: url({{ asset('assets/img/post-3.jpg') }});height: 220px;">
            <div class="overlay-itro"></div>
            <div class="intro-content display-table">
                <div class="table-cell">
                    <div class="container">
                        <h1 class="intro-title mb-4" style="padding-top: 100px;">
                            @yield('title', '')
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="container">
        <!-- Homepage -->
        @if(Route::currentRouteName() == 'homepage')
        <h1 class="my4 text-center" style="padding-top: 20px;">WELCOME TO  IT INDO SCHOOL</h1>
        <hr class="featurette-divider">
        @else
        <!-- end Homepage -->
        <!-- !Homepage -->
        <br>
        @endif
        <!-- end !Homepage -->
        @yield('content')
    </div>
    <hr class="featurette-divider"></hr>
    <footer class="container-fluid">
        <p class="float-left" style="color: #007bff;">&copy; 2018-2020 Copyright By IT INDO SCHOOl &middot; <a href="#"style="color: #007bff;">Privacy</a> &middot; <a href="#" style="color: #007bff;">Terms</a></p>

        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    </footer>
<div id="preloader"></div>
@include('_layouts.footer')