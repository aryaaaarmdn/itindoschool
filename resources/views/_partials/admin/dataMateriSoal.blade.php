@extends('_app.admin', ['isDataMateriActive' => true])

@section('content-header', 'Soal')

@section('breadcrumb')
    <li class="active">
        Soal
    </li>
@endsection

@section('content')
    <!-- Add Soal -->
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                @if(Route::is('admin.data.materi.soal'))
                    <h3 class="box-title">Form Add Soal</h3>
                @elseif(Route::is('admin.data.materi.soal.edit'))
                    <h3 class="box-title">Form Edit Soal</h3>
                @endif
            </div>
            <div class="box-body">
                @if( session('status') )
                    <div class="alert alert-{{ session('alert') }}">
                        {{ session('status') }}
                    </div>
                @endif
                @if(Route::is('admin.data.materi.soal'))
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('admin.data.materi.soal.store') }}" method="POST">
                        @csrf
                        <div class="soal">
                            <h4>Soal / Pertanyaan</h4>
                            <textarea name="soal" id=""></textarea>
                        </div>
                        <div class="jawaban">
                            <div class="jawaban1">
                                <h4>Jawaban A</h4>
                                <textarea name="pilA" id=""></textarea>
                            </div>
                            <div class="jawaban2">
                                <h4>Jawaban B</h4>
                                <textarea name="pilB" id=""></textarea>
                            </div>
                            <div class="jawaban3">
                                <h4>Jawaban C</h4>
                                <textarea name="pilC" id=""></textarea>
                                <input type="hidden" name="data_material_id" value="{{ $data->id }}">
                            </div>
                            <div class="jawaban4">
                                <h4>Jawaban D</h4>
                                <textarea name="pilD" id=""></textarea>
                            </div>
                            <div class="jawaban_asli">
                                <h4>Pilih Jawaban</h4>
                                <select name="jawaban" style="width:100%">
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                </select>
                            </div>
                        </div>
                        <button class="btn btn-md btn-primary" style="margin-top:1em;">
                            Simpan
                        </button>
                    </form>
                @elseif(Route::is('admin.data.materi.soal.edit'))
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('admin.data.materi.soal.update', $data->id) }}" method="POST">
                        @csrf
                        @method('put')
                        <div class="soal">
                            <h4>Soal / Pertanyaan</h4>
                            <textarea name="soal" id="">
                                {{ $data->soal }}
                            </textarea>
                        </div>
                        <div class="jawaban">
                            <div class="jawaban1">
                                <h4>Jawaban A</h4>
                                <textarea name="pilA" id="">
                                    {{ $data->pilA }}
                                </textarea>
                            </div>
                            <div class="jawaban2">
                                <h4>Jawaban B</h4>
                                <textarea name="pilB" id="">
                                    {{ $data->pilB }}
                                </textarea>
                            </div>
                            <div class="jawaban3">
                                <h4>Jawaban C</h4>
                                <textarea name="pilC" id="">
                                    {{ $data->pilC }}
                                </textarea>
                            </div>
                            <div class="jawaban4">
                                <h4>Jawaban D</h4>
                                <textarea name="pilD" id="">
                                    {{ $data->pilD }}
                                </textarea>
                            </div>
                            <div class="jawaban_asli">
                                <h4>Pilih Jawaban</h4>
                                <select name="jawaban" style="width:100%">
                                    <option value="A"{{ $data->jawaban == 'A' ? 'selected' : '' }}>
                                        A
                                    </option>
                                    <option value="B"{{ $data->jawaban == 'B' ? 'selected' : '' }}>
                                        B
                                    </option>
                                    <option value="C"{{ $data->jawaban == 'C' ? 'selected' : '' }}>
                                        C
                                    </option>
                                    <option value="D"{{ $data->jawaban == 'D' ? 'selected' : '' }}>
                                        D
                                    </option>
                                </select>
                            </div>
                        </div>
                        <button class="btn btn-md btn-primary" style="margin-top:1em;">
                            Simpan
                        </button>
                    </form>
                @endif
            </div>
        </div>
    </div>

    <!-- Read Soal -->
    @if(Route::is('admin.data.materi.soal'))
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title" style="margin-bottom:1em;">Data Soal Ujian</h3>
                    <table class="table table-stripped table-bordered table-hover">
                        <thead>
                            <th>Nomor</th>
                            <th>Pertanyaan</th>
                            <th>Pilihan 1</th>
                            <th>Pilihan 2</th>
                            <th>Pilihan 3</th>
                            <th>Pilihan 4</th>
                            <th>Jawaban</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>
                            @php
                                $key = 1;
                            @endphp
                            @foreach($data->questions as $question)
                                <tr>
                                    <td>{{ $key }}</td>
                                    <td>{!! $question->soal !!}</td>
                                    <td>{!! $question->pilA !!}</td>
                                    <td>{!! $question->pilB !!}</td>
                                    <td>{!! $question->pilC !!}</td>
                                    <td>{!! $question->pilD !!}</td>
                                    <td>{!! $question->jawaban !!}</td>
                                    <td>
                                        <a href="{{ route('admin.data.materi.soal.edit', ['id' => $question->id]) }}" class="btn btn-flat btn-primary">
                                            Edit
                                        </a>
                                        <a class="btn btn-flat btn-danger btnDelete deleteDataMateriSoalButton"  
                                            data-target="#deleteData" data-toggle="modal"  
                                            data-id="{{ $question->id }}">
                                            Delete
                                        </a>
                                    </td>
                                </tr>
                                @php
                                    $key++;
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif

@endsection