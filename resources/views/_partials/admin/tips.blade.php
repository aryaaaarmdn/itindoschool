@extends('_app.admin', ['isTipsActive' => true])

@section('content-header', 'Data Tips And Trick')

@section('breadcrumb')
    <li class="active">
        Data Tips And Trick
    </li>
@endsection

@section('content')

    <div class="col-lg-12">
	    <div class="box">
           <div class="box-header">
               <h3 class="box-title">Data Tips And Trick</h3>
           </div>
           <div class="box-body">
               <a href="{{ route('admin.add.tips') }}" class="btn btn-primary btn-md">Tambah</a>
                <br>
                <br>
                @if( session('status') )
                    <div class="alert alert-{{ session('alert') }}">
                        {{ session('status') }}
                    </div>
                @endif
                <table class="table table-stripped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Materi</th>
                            <th>Judul</th>
                            <th>Gambar</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $tips)
                            <tr>
                                <td></td>
                                <td>{{ $tips->material->nama }}</td>
                                <td>{{ $tips->judul }}</td>
                                <td>
                                    <img width="100" 
                                        src="{{ asset('upload/tips') . '/' . $tips->gambar }}"  
                                        alt="image" />
                                </td>
                                <td>
                                    <!-- <a href="#" class="btn btn-danger">Hapus</a> -->
                                    <form action="{{ route('admin.delete.tips', $tips->id) }}" method="post" style="display: inline">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                    <a href="#" class="btn btn-info">Edit</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection