@extends('_app.homepage', ['isMateriActive' => true])

@section('title', $materi->nama)

@section('content')

    <div class="container-fluid">
        <div class="row">
            @foreach($submateri as $submateri)
                <div class="col-md-2">
                    <div class="thumbnail">
                        <a href="{{ route('detail.materi', ['materi' => $materi->nama, 'detail_materi' => $submateri->nama]) }}" target="">
                            <img src="{{ asset('upload/submateri') . '/' . $submateri->img_thumb }}" alt="#" style="width:100%">
                            <div class="caption">
                                {{ $submateri->nama }}
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection