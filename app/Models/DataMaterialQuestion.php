<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataMaterialQuestion extends Model
{
    use HasFactory;

    protected $fillable = [
        'soal', 'pilA', 'pilB', 'pilC', 'pilD', 'jawaban', 'data_material_id'
    ];

    public function datamaterial()
    {
        return $this->belongsTo(DataMaterial::class, 'data_material_id');
    }
}
