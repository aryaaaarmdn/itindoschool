<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_materials', function (Blueprint $table) {
            $table->id();
            $table->string('judul', 255);
            $table->text('isi_materi');
            $table->integer('jml_view')->nullable();
            $table->string('img_thumb', 200)->nullable();
            $table->text('video_materi')->nullable();
            $table->foreignId('sub_material_id')
                    ->constrained()
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->foreignId('admin_id')
                    ->constrained()
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->integer('status_materi')->nullable();
            $table->integer('no_materi');
            $table->text('video_materi2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_materials');
    }
}
