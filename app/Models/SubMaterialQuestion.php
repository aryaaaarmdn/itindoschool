<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubMaterialQuestion extends Model
{
    use HasFactory;

    protected $fillable = [
        'soal', 'pilA', 'pilB', 'pilC', 'pilD', 'jawaban', 'sub_material_id'
    ];

    public function submaterial()
    {
        return $this->belongsTo(SubMaterial::class, 'sub_material_id');
    }

}
