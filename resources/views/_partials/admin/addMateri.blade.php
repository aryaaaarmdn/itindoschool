@extends('_app.admin', ['isMateriActive' => true])

@section('content-header', 'Add Jenis Materi')

@section('breadcrumb')
    <li class="active">
        Add Jenis Materi
    </li>
@endsection

@section('content')

    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-body">
                @if( session('status') )
                    <div class="alert alert-danger">
                        {{ session('status') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{ route('admin.add.materi.proses') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <h4>Nama Jenis materi</h4>
                    <input type="text" class="form-control" name="materi" placeholder="Nama Jenis materi...">
                    <br>
                    <button type="submit" class="btn btn-primary">Tambah Materi</button>
                </form>
            </div>
        </div>
    </div>
   
@endsection