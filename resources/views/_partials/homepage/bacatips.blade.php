@extends('_app.homepage', ['isTipsActive' => true, 'gambar' => $data->gambar])

@section('title', $data->judul)

@section('content')

        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                {!! $data->isi_artikel !!}
            </div>
        </div>
    </div>

    <br>

@endsection