@extends('_app.admin', ['isDataMateriActive' => true])

@section('content-header', 'Data Materi')

@section('breadcrumb')
    <li class="active">
        Data Materi
    </li>
@endsection

@section('content')

    <div class="col-lg-12">
	    <div class="box">
            <div class="box-header">
               <h3 class="box-title">Data Materi</h3>
           </div>
           <div class="box-body">
                <a href="{{ route('admin.add.dataMateri') }}" class="btn btn-primary btn-md">
                    Tambah Materi
                </a>
                <br>
                <br>
                @if( session('status') )
                    <div class="alert alert-{{ session('alert') }}">
                        {{ session('status') }}
                    </div>
                @endif
                <table class="table table-stripped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Materi</th>
                            <th>Jenis Materi</th>
                            <th>Tanggal Post</th>
                            <th>Jumlah View</th>
                            <th>Jumlah Soal</th>
                            <th>Thumbnail</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $materi)
                            <tr>
                                <td></td>
                                <td>{{ $materi->judul }}</td>
                                <td>{{ $materi->submaterial->nama }}</td>
                                <td>{{ $materi->getCreatedAttribute() }}</td>
                                <td>{{ ! $materi->jml_view ? '0' : $materi->jml_view }}</td>
                                <td></td>
                                <td>
                                    <img width="200" 
                                        src="{{ asset('upload/datamateri') . '/' . $materi->img_thumb }}" alt="Materi image">
                                </td>
                                <td>
                                    <a class="btn btn-flat btn-primary" 
                                        href="{{ route('admin.data.materi.soal', $materi->id) }}">
                                        Soal 
                                    </a>
                                    <a class="btn btn-flat btn-primary" 
                                        href="{{ route('admin.edit.datamateri', $materi->id) }}">
                                        Edit Materi
                                    </a>
                                    <a class="btn btn-flat btn-danger btnDelete deleteDataMateriButton" 
                                        data-toggle="modal" 
                                        data-id="{{ $materi->id }}"
                                        data-target="#deleteData">
                                        Delete
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <ul class="pagination justify-content-center" style="margin:10px 40%;">
                    <li class="page-item">
                        <a class="page-link" href="#">
                            Previous
                        </a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#">
                            #
                        </a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#">
                            Next
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>


@endsection