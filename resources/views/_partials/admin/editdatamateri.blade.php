@extends('_app.admin', ['isDataMateriActive' => true])

@section('breadcrumb')
    <li class="active">
        Edit Data Materi
    </li>
@endsection

@section('content')

    <div class="col-lg-12">
	    <div class="box box-primary">
            <div class="box-body">
	            <form action="{{ route('admin.edit.datamateri.proses', $data->id) }}" method="POST" enctype="multipart/form-data">
                    @method('put')
                    @csrf
		            <h4>Judul materi</h4>
                    <input type="text" class="form-control" required name="judul" 
                        value="{{ $data->judul }}" placeholder="Judul materi..." readonly>
                    <h4>Isi materi</h4>
                    <textarea class="form-control" name="isi" placeholder="Isi materi..." required>
                        {{ $data->isi_materi  }}
                    </textarea>
                    <h4>Jenis materi</h4>
                    <select name="submateri" class="form-control" required>
                        <option>-- Pilih Materi --</option>
                        @foreach($submateri as $submateri)
                            <option value="{{ $submateri->id }}"
                                {{ $submateri->id == $data->sub_material_id ? 'selected' : '' }}
                            >
                                {{ $submateri->nama }}
                            </option>
                        @endforeach
                    </select>
                    <h4>Thumbnail materi</h4>
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-6 imgt">
                                <h4>Data Img thumbnail</h4>
                                <img src="{{ asset('upload/datamateri') . '/' . $data->img_thumb }}">
                            </div>
                            <div class="col-lg-6 imgt">
                                <h4>Img thumbnail baru</h4>
                                <img id="dimgthmb" src="">
                            </div>
	                    </div>
	                </div>
	                <input type="file" name="img" class="form-control" id="img-thmb">
	
                    <h4>Video materi </br> .... </h4>
                    <input type="text" class="form-control" name="video" placeholder="Masukan alamat Embed video pada Youtube Channel">

                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-lg-6 video-materi">
                                <h4>Data Video materi</h4>
                                <video controls>
                                    <source src="#">
                                </video>
                            </div>
                        <div class="col-lg-6 video-materi">
                            <h4>Video materi Baru</h4>
                            <video id="dvid" controls>
                                <source src="" type="video/mp4">
                            </video>
                        </div>
                    </div>
                    <input type="file" name="video2" class="form-control" id="vid-materi2">
                    <h4>Status Materi</h4>
                    <select name="status" class="form-control" required>
                        <option>-- Pilih Status Materi --</option>
                        <option value="0" {{ $data->status_materi == 0 ? 'selected' : '' }}>
                            Publish
                        </option>
                        <option value="1" {{ $data->status_materi == 1 ? 'selected' : '' }}>
                            Draf
                        </option>
                    </select>
                    <h4>No Urut Materi</h4>
                    <select name="no_urut" class="form-control" required>
                        <option>-- Pilih No Urut Materi --</option>
                        <option value="1" {{ $data->no_materi == 1 ? 'selected' : '' }}>
                            1
                        </option>
                        <option value="2" {{ $data->no_materi == 2 ? 'selected' : '' }}>
                            2
                        </option>
                        <option value="3" {{ $data->no_materi == 3 ? 'selected' : '' }}>
                            3
                        </option>
                        <option value="4" {{ $data->no_materi == 4 ? 'selected' : '' }}>
                            4
                        </option>
                        <option value="5" {{ $data->no_materi == 5 ? 'selected' : '' }}>
                            5
                        </option>
                        <option value="6" {{ $data->no_materi == 6 ? 'selected' : '' }}>
                            6
                        </option>
                        <option value="7" {{ $data->no_materi == 7 ? 'selected' : '' }}>
                            7
                        </option>
                        <option value="8" {{ $data->no_materi == 8 ? 'selected' : '' }}>
                            8
                        </option>
                        <option value="9" {{ $data->no_materi == 9 ? 'selected' : '' }}>
                            9
                        </option>
                        <option value="10" {{ $data->no_materi == 10 ? 'selected' : '' }}>
                            10
                        </option>
                    </select>
                    <br>
                    <button type="submit" class="btn btn-primary">
                        Simpan Materi
                    </button>
	            </form>
            </div>
        </div>
    </div>

@endsection