@extends('_app.homepage', ['isHomeActive' => true])

@section('content')
    <div class="row">
        @foreach($materi as $materi)
            <div class="col-lg-4 mb-4">
                <div class="card h-100">
                    @auth
                        <a href="{{ route('materi', $materi->nama) }}">
                    @endauth
                    @guest
                        <a href="{{ route('homepage') }}">
                    @endguest
                            @if($materi->nama == 'Programming')
                                <img class="card-img-top" src="{{ asset('assets/img/programer.png') }}" 
                                    alt="{{ $materi->nama }}">
                            @elseif($materi->nama == 'Linux')
                                <img class="card-img-top" src="{{ asset('assets/img/jaringan.png') }}"
                                    alt="{{ $materi->nama }}">
                            @elseif($materi->nama == 'Design')
                                <img class="card-img-top" src="{{ asset('assets/img/design.jpg') }}"
                                    alt="{{ $materi->nama }}">
                            @elseif($materi->nama == 'Robotik')
                                <img class="card-img-top" src="{{ asset('assets/img/robotik.jpg') }}" 
                                    alt="{{ $materi->nama }}">
                            
                            @else 
                                <img class="card-img-top" src="{{ asset('assets/img/materidefault.jpeg') }}" 
                                    alt="{{ $materi->nama }}">
                            @endif
                        </a>
                    <div class="card-body">
                        <h4><b>
                            @auth
                                <a href="{{ route('materi', $materi->nama) }}">
                                    {{ $materi->nama }}
                                </a>
                            @endauth
                            @guest
                                <a href="{{ route('homepage') }}">
                                    {{ $materi->nama }}
                                </a>
                            @endguest
                        </b></h4>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection