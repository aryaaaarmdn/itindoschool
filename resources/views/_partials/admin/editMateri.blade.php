@extends('_app.admin', ['isMateriActive' => true])

@section('content-header', 'Edit Jenis Materi')

@section('content')

    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-body">
                @if( session('status') )
                    <div class="alert alert-danger">
                        {{ session('status') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{ route('admin.edit.materi.proses', $data->id) }}" method="POST">
                    @method('put')
                    @csrf
                    <div class="form-group">
                        <h4>Judul materi</h4>
                        <input type="text" name="materi" class="form-control" value="{{ old('materi', $data->nama) }}">
                        <!-- <input type="hidden" name="id" value="{{ $data->id }}"> -->
                    </div>
                    <button type="submit" class="btn btn-primary">Edit Jenis Materi</button>
                </form>
    </div>

@endsection
