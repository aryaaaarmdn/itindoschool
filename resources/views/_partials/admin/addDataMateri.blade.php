@extends('_app.admin', ['isDataMateriActive' => true])

@section('content-header', 'Add Data Materi')

@section('breadcrumb')
    <li class="active">
        Add Data Materi
    </li>
@endsection

@section('content')

    <div class="col-lg-12">
	    <div class="box box-primary">
            <div class="box-body">
                @if( session('status') )
                    <div class="alert alert-danger">
                        {{ session('status') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
	            <form action="{{ route('admin.add.dataMateri.proses') }}" method="POST" enctype="multipart/form-data">
                    @csrf
		            <h4>Judul materi</h4>
                    <input type="text" class="form-control" name="judul" placeholder="Judul materi...">
                    <h4>Isi materi</h4>
                    <textarea class="form-control" name="isi" placeholder="Isi materi...">
                    </textarea>
		            <h4>Jenis materi</h4>
		            <select name="submateri" id="type_materi" class="form-control">
			            <option>-- Pilih Submateri --</option>
                        @foreach($data as $submateri)
                            <option value="{{ $submateri->id }}">
                                {{ $submateri->nama }}
                            </option>
                        @endforeach
		            </select>
		            <h4>No Urut Materi</h4>
                    <select name="nomor_urut" class="form-control" required>
                        <option>-- Pilih No Urut Materi --</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                    </select>
                    <div class="col-lg-6">
                        <h4>Thumbnail materi</h4>
                        <img id="dimgthmb" width="250px" height="250px" src="">
                        <br>
                        <input type="file" name="img"  id="img-thmb" class="form-control">
                    </div>
                    <div class="col-lg-6">
                        <h4>Video materi</h4>
                        <video id="dvid" width="250px" height="250px" controls>
                            <source src="" type="video/mp4">
                        </video>
                        <input type="text" class="form-control" name="video" placeholder="Masukan alamat Embed video pada Youtube Channel">
                        <input type="file" name="video2" id="vid-materi2" class="form-control">
                    </div>
                    <h4>Status Materi</h4>
                    <select name="status" class="form-control">
                        <option value="">-- Pilih Status Materi --</option>
                        <option value="0">Publish</option>
                        <option value="1">Draf</option>
                    </select>
		            <br>
		            <button type="submit" class="btn btn-primary">Tambah Materi</button>
	            </form>
            </div>
        </div>
    </div>

@endsection