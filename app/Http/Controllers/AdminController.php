<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Material;
use App\Models\SubMaterial;
use App\Models\SubMaterialQuestion;
use App\Models\DataMaterial;
use App\Models\DataMaterialQuestion;
use App\Models\User;
use App\Models\Tip;

class AdminController extends Controller
{
    
    public function index()
    {
        return view('_partials.admin.dashboard');
    }

    public function login()
    {
        return view('_partials.admin.login');
    }

    public function login_process(Request $request)
    {
        $credentials = $request->only('username', 'password');

        if (Auth::guard('admin')->attempt($credentials)) {
            $request->session()->regenerate();

            // return redirect()->intended(route('admin.dashboard'));
            return redirect()->route('admin.dashboard');
        }

        return back();
    }

    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();

        // $request->session()->invalidate();

        // $request->session()->regenerateToken();

        return redirect()->route('admin.login');
    }

    public function materi()
    {
        $materi = Material::all();
        return view('_partials.admin.materi', ['materi' => $materi]);
    }

    public function addMateri()
    {
        return view('_partials.admin.addMateri');
    }

    public function addMateriProses(Request $request)
    {
        $request->validate([
            'materi' => 'required',
        ]);

        $materi = new Material;
        $materi->nama = $request->materi;
        $save = $materi->save();

        if( $save ) {
            return redirect()->route('admin.materi')
                    ->with('status', 'Berhasil Menambahkan Materi')
                    ->with('alert', 'success');
        } else {
            return redirect()->back()
                    ->with('status', 'Gagal Menambahkan Materi')
                    ->with('alert', 'danger');
        }
    }

    public function editMateri($id)
    {
        $data = Material::findOrFail($id);
        return view('_partials.admin.editMateri', ['data' => $data]);
    }

    public function editMateriProses(Request $request, $id)
    {
        $request->validate([
            'materi' => 'required',
        ]);

        $materi = Material::findOrFail($id);
        $materi->nama = $request->materi;

        $save = $materi->save();

        if( $save ) {
            return redirect()->route('admin.materi')
                ->with('status', 'Berhasil Edit Materi')
                ->with('alert', 'success');
        } else {
            return redirect()->back()
                ->with('status', 'Gagal Edit Materi')
                ->with('alert', 'danger');
        }
    }

    public function deleteMateri(Request $request, $id)
    {

        if ( Material::destroy($id) ) {
            return redirect()->route('admin.materi')
                ->with('status', 'Berhasil Hapus Materi')
                ->with('alert', 'success');
        } else {
            return redirect()->back()
                ->with('status', 'Gagal Hapus Materi', 'danger')
                ->with('alert', 'danger');
        }
    }

    public function subMateri()
    {
        $submateri = SubMaterial::all();

        return view('_partials.admin.submateri', ['data' => $submateri]);
    }

    public function addSubmateri()
    {
        $jenisMateri = Material::all();
        return view('_partials.admin.addSubMateri', ['data' => $jenisMateri]);
    }

    public function addSubMateriProses(Request $request)
    {
        $submateri = new SubMaterial;

        if ( $request->hasFile('img') ) {
            $submateri->img_thumb = $request->file('img')->hashName();
            $request->file('img')
                    ->move(public_path('upload/submateri/'), $request->file('img')->hashName());
        }

        $submateri->nama = $request->nama;
        $submateri->material_id = $request->id_materi;
        $save = $submateri->save();

        if( $save ) {
            return redirect()->route('admin.submateri')
                    ->with('status', 'Berhasil Menambahkan Sub Materi')
                    ->with('alert', 'success');
        } else {
            return redirect()->back()
                    ->with('status', 'Gagal Menambahkan Sub Materi')
                    ->with('alert', 'danger');
        }
    }

    public function deleteSubMateri(Request $request, $id)
    {
        unlink(public_path('upload/submateri/') . SubMaterial::find($id)->img_thumb);
        if ( SubMaterial::destroy($id) ) {
            return redirect()->route('admin.submateri')
                ->with('status', 'Berhasil Hapus Sub Materi')
                ->with('alert', 'success');
        } else {
            return redirect()->back()
                ->with('status', 'Gagal Hapus Sub Materi', 'danger')
                ->with('alert', 'danger');
        }
    }

    public function editSubMateri($id)
    {
        $submateri = SubMaterial::findOrFail($id);
        $materi = Material::all();

        return view('_partials.admin.editSubMateri', ['data' => $submateri, 'materi' => $materi]);
    }

    public function editSubMateriProses(Request $request, $id)
    {
        $submateri = SubMaterial::findOrFail($id);
        $submateri->nama = $request->nama;
        $submateri->material_id = $request->id_materi;

        if ( $request->hasFile('img') ) {
            unlink(public_path('upload/submateri/') . $submateri->img_thumb);
            $submateri->img_thumb = $request->file('img')->hashName();
            $request->file('img')
                    ->move(public_path('upload/submateri/'), $request->file('img')->hashName());
        } 

        $save = $submateri->save();

        if( $save ) {
            return redirect()->route('admin.submateri')
                ->with('status', 'Berhasil Edit Sub Materi')
                ->with('alert', 'success');
        } else {
            return redirect()->route('admin.submateri')
                ->with('status', 'Gagal Edit Sub Materi')
                ->with('alert', 'danger');
        }
    }

    public function SubmateriSoal(SubMaterial $id)
    {
        dd($id->questions());
        return view('_partials.admin.submateriSoal', ['data' => $id]);
    }

    public function AddSubmateriSoalProses(Request $request)
    {
        $request->validate([
            'soal' => 'required',
            'pilA' => 'required',
            'pilB' => 'required',
            'pilC' => 'required',
            'pilD' => 'required',
            'jawaban' => 'required',
            'submaterial_id' => 'required'
        ]);

        $SubMaterialQuestion = SubMaterialQuestion::create([
            'soal' => $request->soal,
            'pilA' => $request->pilA,
            'pilB' => $request->pilB,
            'pilC' => $request->pilC,
            'pilD' => $request->pilD,
            'jawaban' => $request->jawaban,
            'sub_material_id' => $request->submaterial_id
        ]);

        if( $SubMaterialQuestion ) {
            return redirect()->back()
                    ->with('status', 'Berhasil Menambahkan Soal Ujian')
                    ->with('alert', 'success');
        } else {
            return redirect()->back()
                    ->with('status', 'Gagal Menambahkan Soal Ujian')
                    ->with('alert', 'danger');
        }

    }

    public function SubmateriSoalEdit(SubMaterialQuestion $id)
    {
        return view('_partials.admin.submateriSoal', ['data' => $id]);
    }

    public function SubmateriSoalEditProses(Request $request, SubMaterialQuestion $id)
    {
        $request->validate([
            'soal' => 'required',
            'pilA' => 'required',
            'pilB' => 'required',
            'pilC' => 'required',
            'pilD' => 'required',
            'jawaban' => 'required',
        ]);

        $data = [
            'soal' => $request->soal,
            'pilA' => $request->pilA,
            'pilB' => $request->pilB,
            'pilC' => $request->pilC,
            'pilD' => $request->pilD,
            'jawaban' => $request->jawaban
        ];

        if ( $id->update($data) ) {
            return redirect()->route('admin.submateri.soalujian', ['id' => $id->submaterial->id])
                ->with('status', 'Berhasil Edit Soal Ujian')
                ->with('alert', 'success');
        } else {
            return redirect()->route('admin.submateri.soalujian', ['id' => $id->submaterial->id])
                ->with('status', 'Gagal Edit Soal Ujian')
                ->with('alert', 'danger');
        }

    }

    public function subMateriSoalDelete(SubMaterialQuestion $id)
    {
        if ( $id->delete() ) {
            return redirect()->back()
                ->with('status', 'Berhasil Hapus Soal Ujian')
                ->with('alert', 'success');
        } else {
            return redirect()->back()
                ->with('status', 'Gagal Hapus Soal Ujian')
                ->with('alert', 'danger');
        }   
    }

    public function DataMateriSoal(DataMaterial $id)
    {
        return view('_partials.admin.dataMateriSoal', ['data' => $id]);
    }

    public function DataMateriSoalStore(Request $request)
    {
        $request->validate([
            'soal' => 'required',
            'pilA' => 'required',
            'pilB' => 'required',
            'pilC' => 'required',
            'pilD' => 'required',
            'jawaban' => 'required',
            'data_material_id' => 'required'
        ]);

        $DataMaterialQuestion = DataMaterialQuestion::create([
            'soal' => $request->soal,
            'pilA' => $request->pilA,
            'pilB' => $request->pilB,
            'pilC' => $request->pilC,
            'pilD' => $request->pilD,
            'jawaban' => $request->jawaban,
            'data_material_id' => $request->data_material_id
        ]);

        if( $DataMaterialQuestion ) {
            return redirect()->back()
                    ->with('status', 'Berhasil Menambahkan Soal')
                    ->with('alert', 'success');
        } else {
            return redirect()->back()
                    ->with('status', 'Gagal Menambahkan Soal')
                    ->with('alert', 'danger');
        }
    }

    public function DataMateriSoalEdit(DataMaterialQuestion $id)
    {
        return view('_partials.admin.dataMateriSoal', ['data' => $id]);
    }

    public function DataMateriSoalUpdate(Request $request, DataMaterialQuestion $id)
    {
        $request->validate([
            'soal' => 'required',
            'pilA' => 'required',
            'pilB' => 'required',
            'pilC' => 'required',
            'pilD' => 'required',
            'jawaban' => 'required',
        ]);

        $data = [
            'soal' => $request->soal,
            'pilA' => $request->pilA,
            'pilB' => $request->pilB,
            'pilC' => $request->pilC,
            'pilD' => $request->pilD,
            'jawaban' => $request->jawaban
        ];

        if ( $id->update($data) ) {
            return redirect()->route('admin.data.materi.soal', ['id' => $id->datamaterial->id])
                ->with('status', 'Berhasil Edit Soal')
                ->with('alert', 'success');
        } else {
            return redirect()->route('admin.data.materi.soal', ['id' => $id->datamaterial->id])
                ->with('status', 'Gagal Edit Soal')
                ->with('alert', 'danger');
        }

    }

    public function DataMateriSoalDelete(DataMaterialQuestion $id)
    {
        if ( $id->delete() ) {
            return redirect()->back()
                ->with('status', 'Berhasil Hapus Soal Ujian')
                ->with('alert', 'success');
        } else {
            return redirect()->back()
                ->with('status', 'Gagal Hapus Soal Ujian')
                ->with('alert', 'danger');
        }   
    }


    public function showUsers()
    {
        $users = User::all();
        return view('_partials.admin.user', ['data' => $users]);
    }

    public function deleteUser($id)
    {
        if ( User::destroy($id) ) {
            return redirect()->route('admin.users')
                ->with('status', 'Berhasil Hapus Data User')
                ->with('alert', 'success');
        } else {
            return redirect()->back()
                ->with('status', 'Gagal Hapus Data User', 'danger')
                ->with('alert', 'danger');
        }
    }

    public function editUser($id)
    {
        $user = User::findOrFail($id);
        return view('_partials.admin.editUser', ['data' => $user]);
    }

    public function editUserProses(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $data = [
            'nama' => $request->filled('nama') ? 
                        $request->nama : $user->nama,
            'email' => $request->filled('email') ? 
                            $request->email : $user->email,
            'password' => $request->filled('password') ? 
                            Hash::make($request->password) : $user->password,
        ];

        $update = $user->update($data);
        if ( $update ) {
            return redirect()->route('admin.users')
                    ->with('status', 'Berhasil Edit Data User')
                    ->with('alert', 'success');
        } else {
            return redirect()->back()
                    ->with('status', 'Gagal Edit Data User')
                    ->with('alert', 'danger');
        }
    }

    public function dataMateri()
    {
        $submateri = DataMaterial::all();
        return view('_partials.admin.datamateri', ['data' => $submateri]);
    }

    public function addDataMateri()
    {
        $submateri = SubMaterial::all();

        return view('_partials.admin.addDataMateri', ['data' => $submateri]);
    }

    public function addDataMateriProses(Request $request)
    {
        [
            $judul, $isi, $submateri_id, $nomor_urut, $img, $video, $video2, $status
        ] = [
            $request->judul,
            $request->isi,
            $request->submateri,
            $request->nomor_urut,
            $request->hasFile('img') ? $request->file('img')->hashName() : null,
            $request->filled('video') ? $request->video : null,
            $request->hasFile('video2') ? $request->file('video2')->hashName() : null,
            $request->has('status') ? $request->status : null
        ];

        $dataMateri = new DataMaterial;

        $dataMateri->judul = $judul;
        $dataMateri->isi_materi = $isi;
        $dataMateri->img_thumb = $img;
        $dataMateri->video_materi = $video;
        $dataMateri->sub_material_id = $submateri_id;
        $dataMateri->admin_id = Auth::id();
        $dataMateri->status_materi = $status;
        $dataMateri->no_materi = $nomor_urut;
        $dataMateri->video_materi2 = $video2;

        if ( $request->hasFile('img') ) {
            $request->file('img')
                    ->move(public_path('upload/datamateri/img/'), $request->file('img')->hashName());
        }

        if ( $request->hasFile('video2') ) {
            $request->file('video2')
                    ->move(public_path('upload/datamateri/video/'), $request->file('video2')->hashName());
        }

        if ( $dataMateri->save() ) {
            return redirect()->route('admin.dataMateri')
                ->with('status', 'Berhasil Menambahkan Materi')
                ->with('alert', 'success');
        } else {
            return redirect()->back()
                ->with('status', 'Gagal Menambahkan Materi')
                ->with('alert', 'danger');
        }   
    }

    public function deleteDataMateri($id)
    {
        if ( file_exists(public_path('upload/datamateri/') . DataMaterial::find($id)->img_thumb) ) {
            if ( DataMaterial::find($id)->img_thumb ) {
                unlink(public_path('upload/datamateri/') . DataMaterial::find($id)->img_thumb);
            }
        }

        if ( DataMaterial::destroy($id) ) {
            return redirect()->route('admin.dataMateri')
                ->with('status', 'Berhasil Hapus Data User')
                ->with('alert', 'success');
        } else {
            return redirect()->back()
                ->with('status', 'Gagal Hapus Data User', 'danger')
                ->with('alert', 'danger');
        }
    }

    public function editDataMateri($id)
    {
        $materi = DataMaterial::findOrFail($id);
        $submateri = SubMaterial::all();
        return view('_partials.admin.editdatamateri', ['data' => $materi, 'submateri' => $submateri]);
    }

    public function editDataMateriProses(Request $request, $id)
    {

        $dataMateri = DataMaterial::find($id);

        $data = [
            'judul' => $request->judul,
            'isi_materi' => $request->isi,
            'img_thumb'=> $request->hasFile('img') ? 
                                $request->file('img')->hashName() :
                                ($dataMateri->img_thumb ?
                                    $dataMateri->img_thumb :
                                    null),
            'video_materi' => $request->video,
            'sub_material_id' => $request->submateri,
            'status_materi' => $request->status,
            'no_materi' => $request->no_urut,
            'video_materi2' => $request->hasFile('video2') ? 
                                    $request->file('video2')->hashName() :
                                    ($dataMateri->video_materi2 ?
                                        $dataMateri->video_materi2 :
                                        null),
        ];

        if ( $request->hasFile('img') ) {
            if ( $dataMateri->img_thumb === null ) {
                $request->file('img')
                        ->move(public_path('upload/datamateri/img/'), $request->file('img')->hashName());
            } else { 
                unlink(public_path('upload/datamateri/') . $dataMateri->img_thumb);
                $request->file('img')
                        ->move(public_path('upload/datamateri/img/'), $request->file('img')->hashName());
            }
        }

        if ( $request->hasFile('video2') ) {
            if ( $dataMateri->video_materi2 === null ) {
                $request->file('video2')
                        ->move(public_path('upload/datamateri/video/'), $request->file('video2')->hashName());
            } else { 
                unlink(public_path('upload/datamateri/video/') . $dataMateri->video_materi2);
                $request->file('video2')
                        ->move(public_path('upload/datamateri/video/'), $request->file('video2')->hashName());
            }
        }

        if ( $dataMateri->update($data) ) {
            return redirect()->route('admin.dataMateri')
                    ->with('status', 'Data Materi Berhasil di Ubah')
                    ->with('alert', 'success');
        } else {
            return redirect()->back()
                    ->with('status', 'Data Materi Gagal di Ubah')
                    ->with('alert', 'danger');
        }        
    }

    public function Tips()
    {
        $tips = Tip::all();
        return view('_partials.admin.tips', ['data' => $tips]);
    }

    public function addTips()
    {
        $jenisMateri = Material::all();

        return view('_partials.admin.addTips', ['data' => $jenisMateri]);
    }

    public function addTipsProses(Request $request)
    {
        $tips = new Tip;

        $tips->material_id = $request->id_materi;
        $tips->judul = $request->judul;
        $tips->gambar = $request->hasFile('img') ? 
                            $request->file('img')->hashName() : null;
        $tips->isi_artikel = $request->isi;

        if ( $request->hasFile('img') ) {
            $request->file('img')
                    ->move(public_path('upload/tips/'), $request->file('img')->hashName());
        }

        if ( $tips->save() ) {
            return redirect()->route('admin.tips')
                    ->with('status', 'Tips & Trick Berhasil Ditambah')
                    ->with('alert', 'success');
        } else {
            return redirect()->back()
                    ->with('status', 'Tips & Trick Gagal Ditambah')
                    ->with('alert', 'danger');
        }

    }

    public function tipsDelete($id)
    {
        if ( file_exists(public_path('upload/tips/') . Tip::find($id)->gambar) ) {
            if ( Tip::find($id)->gambar ) {
                unlink(public_path('upload/tips/') . Tip::find($id)->gambar);
            }
        }

        if ( Tip::destroy($id) ) {
            return redirect()->back()
                ->with('status', 'Berhasil Hapus Data Tips & Trick')
                ->with('alert', 'success');
        } else {
            return redirect()->back()
                ->with('status', 'Gagal Hapus Data Tips & Trick', 'danger')
                ->with('alert', 'danger');
        }
    }

}
