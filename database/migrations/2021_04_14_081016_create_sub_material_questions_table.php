<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubMaterialQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_material_questions', function (Blueprint $table) {
            $table->id();
            $table->text('soal');
            $table->string('pilA', 200);
            $table->string('pilB', 200);
            $table->string('pilC', 200);
            $table->string('pilD', 200);
            $table->char('jawaban', 2);
            $table->foreignId('sub_material_id')
                    ->constrained()
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_material_questions');
    }
}
