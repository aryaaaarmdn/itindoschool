@extends('_app.homepage', ['isMateriActive' => true])

@section('content')
    <div class="container-fluid">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('homepage') }}">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <a href="{{ route('materi', $breadcrumbs1) }}">{{ $breadcrumbs1 }}</a>
            </li>
            <li class="breadcrumb-item active">
                {{ $breadcrumbs2 }}
            </li>
        </ol>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2">
                <div class="card">
                    <div class="card-header">
                        <div class="md-0">
                            List Materi
                        </div>
                    </div>
                    <div class="card-body">
                        @foreach($total_materi->sortBy('no_materi') as $materi)
                            <h5 class="mb-0">
                                <button class="btn btn-link text-center">
                                    <a href="{{ route('detail.materi', ['materi' => $breadcrumbs1, 'detail_materi' => $breadcrumbs2, 'index' => $materi->no_materi]) }}">Materi {{$materi->no_materi}} </a>
                                </button>
                            </h5>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-lg-10">
                @if( $dataMateri !== null )
                    <div class="video-materi">
                        <table width="100%">
                            <tr>
                                @if( $dataMateri->video_materi !== null )
                                    <td>
                                        <iframe width="560" height="315" 
                                        src="https://www.youtube.com/embed/NBZ9Ro6UKV8" 
                                        title="YouTube video player"
                                        frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </td>
                                @endif
                                @if( $dataMateri->video_materi2 !== null )
                                    <td>
                                        <video controls>
                                            <source src="{{ asset('upload/datamateri/video') . '/' . $dataMateri->video_materi2}}" 
                                                type="video/mp4">
                                        </video>
                                    </td>
                                @endif
                            </tr>
                        </table>
                    </div>
                @else
                    <h1 style="text-align: center;">Materi Sedang Dalam Proses</h1>
                @endif
            </div>
        </div>
    </div>
    <br>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2"></div>
            @if( $dataMateri  !== null)
                <div class="col-lg-10">
                    <div class="card">
                        <div class="card-header">
                                <h5 class="text-center">{{ $dataMateri->judul }}</h5>
                        </div>
                        <div class="card-body">
                            {!! $dataMateri->isi_materi !!}
                        </div>
                    </div>
                    <!-- <div class="card">
                        <div id="ujianx" style="display: block;">
                            <form action="#" id="pertanyaan" method="POST">
                                <div class="card-header">
                                    <h5 class="text-left">SOAL</h5>
                                </div>
                                <div class="card-body">
                                    <table>
                                        <tr>
                                            <td>
                                                #
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <input type="radio" name="sen" id="sen1" onclick="cek_soal()" value="A"/>
                                                        </td>
                                                        <td>#</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="radio" name="sen" id="sen2" onclick="cek_soal()" value="B"/>
                                                        </td>
                                                        <td>#</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="radio" name="sen" id="sen3" onclick="cek_soal()" value="C"/>
                                                        </td>
                                                        <td>#</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="radio" name="sen" id="sen4" onclick="cek_soal()" value="D"/>
                                                        </td>
                                                        <td>#</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="jawabannya" id="jawabannya" value="#" />
                                                <input type="hidden" name="pilihan" id="pilihan" />
                                                <input type="hidden" name="materi" id="materi" value="#" />
                                                <input type="hidden" name="no_materi" id="no_materi" value="#" />
                                                <input type="button" class="btn btn-md btn-primary" value="Jawab" onclick="cek_jawab()">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div id="ujian" style="display: none;">
                            <form action="#" id="pertanyaanujian" method="POST">
                                <div class="card-header">
                                    <h5 class="text-left">SOAL UJIAN</h5>
                                </div>         
                                <div class="card-body">
                                    <table>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <input type="radio" name="sen#" id="sen1#" onclick="cek_soal_ujian()" value="A"/>
                                                                    </td>
                                                                    <td>#</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <input type="radio" name="sen#" id="sen2#" onclick="cek_soal_ujian()" value="B"/>
                                                                    </td>
                                                                    <td>#</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <input type="radio" name="sen#" id="sen3#" onclick="cek_soal_ujian()" value="C"/>
                                                                    </td>
                                                                    <td>#</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <input type="radio" name="sen#" id="sen4#" onclick="cek_soal_ujian()" value="D"/>
                                                                    </td>
                                                                    <td>#</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="hidden" name="id_soal#" id="id_soal#" value="#" />
                                                            <input type="hidden" name="jawabannya#" id="jawabannya#" value="#" />
                                                            <input type="hidden" name="pilihan#" id="pilihan#" />
                                                            <input type="hidden" name="materi" id="materi" value="#" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="button" class="btn btn-md btn-primary" value="Jawab" onclick="cek_jawabujian()">
                                            </td>
                                        </tr>
                                    </table>           
                                </div>
                            </form>
                        </div>
                    </div> -->
                </div>
            @endif
        </div>
    </div>
    
@endsection
<!-- End of Chaport Live Chat code -->