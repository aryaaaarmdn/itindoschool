@extends('_app.homepage')

@section('content')

<div class="container">
	<div class="media border p-3">
        <!-- Jika ada foto pada database-->
		@if(!auth()->user()->img == null)
            <img src="{{ asset('upload/ava') . '/' . Auth::user()->img }}" 
				alt="Ava" style="width:100px;">
        <!-- Jika Gaada foto -->
		@else
            <img src="{{ asset('assets/images/koala2.jpg') }}" 
				alt="gaada profil" style="width:100px;">
		@endif
        <!-- Akhir dr jika -->
			<div class="media-body">
				<table>
			  		<tr>
			  			<td>
			  				<a href="{{ route('editProfile') }}">
                              <img src="{{ asset('assets/img/settings.png') }}"href="" style="width:20px;"> 
                              Ubah Profile 
                            </a>
			  			</td>
			  			<td>
			  			</td>
			  		</tr>
			  		<tr>
			  			<td>
			  				 Nama 
			  			</td>
			  			<td>
			  				 {{ Auth::user()->nama }}
			  			</td>
			  		</tr>
			  		<tr>
			  			<td>
			  				 E-mail  
			  			</td>
			  			<td>
						  {{ Auth::user()->email }}
			  			</td>
			  		</tr>
			  		<tr>
			  			<td>
			  				 No.Telp / HP  
			  			</td>
			  			<td>
						  	@if(Auth::user()->no_telp)
						  		{{ Auth::user()->no_telp }}
							@else
								Belum Di setting
							@endif
			  			</td>
			  		</tr>
			  	</table>
			</div>
	</div>
</div>

<div class="container">
	<ul class="nav nav-tabs">
		<li class="nav-item" >
			<a class="nav-link active" data-toggle="tab" href="#activitas">ACTIVITAS</a>
	  	</li>
	  	<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#skill">SKILL</a>
	  	</li>
	  	<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#badges">BADGES</a>
	  	</li>
	  	<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#leaderboard">LEADERBOARD</a>
	  	</li>
	</ul>

	<!-- NAVIGASI CONTENT-->
	<div class="tab-content">
		<div class="tab-pane container active" id="activitas">
			<table width="70%"  align="center"  border="1">

				<tr>
					<td><b><center>No.</center></b></td>
					<td><b><center>Tanggal</center></b></td>
					<td><b><center>Desc</center></b></td>
				</tr>
				<tr>
					<td><center></center></td>
					<td><center></center></td>
					<td></td>
				</tr>
			</table>
		</div>

		<!-- SKILL -->	  
	  	<div class="tab-pane container fade" id="skill">
          <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Distinctio voluptatem voluptates odit autem hic dolorum et labore quisquam mollitia dolorem.</p>
	  	</div>
		<div class="tab-pane container fade" id="badges"> 
			<br>

			<script>
				$(document).ready(function(){
				$('[data-toggle="tooltip"]').tooltip();   
				});
			</script>
		</div>
		<div class="tab-pane container fade" id="leaderboard">
			<table class="table table-stripped table-bordered table-hover">
				<thead>
					<tr>
					<th>Peringkat</th>
					<th>Nama </th>
					<th>Foto</th>
					</tr>
				</thead>
				<tbody>
					<td></td>
					<td></td>
					<td></td>
				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- Begin of Chaport Live Chat code -->
    <script type="text/javascript">
		(function(w,d,v3){
		w.chaportConfig = { appId : '5cb80676db2a1b411c9d76ec' };

		if(w.chaport)return;v3=w.chaport={};v3._q=[];v3._l={};v3.q=function(){v3._q.push(arguments)};v3.on=function(e,fn){if(!v3._l[e])v3._l[e]=[];v3._l[e].push(fn)};var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://app.chaport.com/javascripts/insert.js';var ss=d.getElementsByTagName('script')[0];ss.parentNode.insertBefore(s,ss)})(window, document);
	</script>
	<!-- End of Chaport Live Chat code -->
@endsection