$(document).ready(function(){
	function changeImg(){
		$('#img-thmb').change(function(){
		if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#dimgthmb')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(this.files[0]);
            }
		})
	}

	function changeVid(){
		$('#vid-materi').change(function(){
			if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#dvid')
                        .attr('src', e.target.result);
                };
                console.log($('#dvid'));
                reader.readAsDataURL(this.files[0]);
            }
		})
	}

    function AddSoal(){
        $("#btnAdd").click(function(){
           valsoal = $("#jmlNew").val();
           for (var i = 1; i <= valsoal; i++) {
             $("#addnewsoal").append("<div class='form-group'><label>Soal</label><textarea class='form-control' name='soal[]'></textarea></div><div class='form-group'><label>Pilihan A</label><textarea class='form-control' name='pilA[]'></textarea></div><div class='form-group'><label>Pilihan B</label><textarea class='form-control' name='pilB[]'></textarea></div><div class='form-group'><label>Pilihan C</label><textarea class='form-control' name='pilC[]'></textarea></div><div class='form-group'><label>Pilihan D</label><textarea class='form-control' name='pilD[]'></textarea></div>");
           }
        })
    }

    function deleteData() {
        $('.btnDelete').click(function(){
      $('#deleteBtn').attr('href',$(this).attr('data-href'));
        })
    }

    // AddSoal();
    deleteData();
    changeImg();
	changeVid();
})