<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Dashboard</h1>
    <form method="POST" action="{{ route('logout') }}">
        @csrf
        <a class="nav-link" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                    this.closest('form').submit();">
            <i class="fa fa-sign-out" aria-hidden="true"></i> 
            Logout
        </a>
    </form>
    @if(Auth::guard('web')->check())
        <p style="background-color:green">Logged in as a user</p>
    @else
        <p style="background-color:red">Logged out as a user</p>
    @endif

    @if(Auth::guard('admin')->check())
        <p style="background-color:green">Logged in as a admin</p>
    @else
        <p style="background-color:red">Logged out as a admin</p>
    @endif
</body>
</html>