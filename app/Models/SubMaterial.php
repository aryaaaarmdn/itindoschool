<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubMaterial extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'nama',
        'img_thumb',
        'material_id'
    ];

    public function material()
    {
        return $this->belongsTo(Material::class);
    }

    public function dataMaterials()
    {
        return $this->hasMany(DataMaterial::class);
    }

    public function questions()
    {
        return $this->hasMany(SubMaterialQuestion::class);
    }
}
