@extends('_app.homepage')

@section('content')
    <div class="container">
        @if(session()->has('msg'))
            <div class="alert alert-{{ session('msg')[0] }}">
                {{ session('msg')[1] }}
            </div>
        @endif
		<div class="media border p-3">
            <table>
                <tr>
                    <td>
                        @if(!auth()->user()->img == null)
                            <img src="{{ asset('upload/ava') . '/' . Auth::user()->img }}" 
                                alt="User profile picture" style="width:100px;">
                        <!-- Jika Gaada foto -->
                        @else
                            <img src="{{ asset('assets/images/koala2.jpg') }}" 
                                alt="User profile picture" style="width:100px;">
                        @endif
                    </td>
                    <td width="10px"></td>
                    <td>
                        Data Profile
                    </td>
                </tr>
          
                <tr>
                    <td></td>
                    <td width="10px"></td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <div class="media-body">
                                        <form  action="{{ route('updateProfile') }}" method="POST" enctype="multipart/form-data">
                                            @method('PUT')
                                            @csrf
                                            <div class="form-group">
                                                <label for="user">Nama</label>
                                                    <input type="text" class="form-control"
                                                            id="nama" 
                                                            placeholder="{{ Auth::user()->nama }}" 
                                                            name="nama">
                                            </div>
                                            <div class="form-group">
                                                <label for="email">E-mail</label>
                                                <input type="email" class="form-control" id="email" placeholder="{{ Auth::user()->email }}" name="email">
                                            </div>
                                            <div class="form-group">
                                                <label for="pwd">Password:</label>
                                                <input type="password" class="form-control"
                                                        id="password" placeholder="***********" name="password">
                                            </div>
                                            <div class="form-group">
                                                <label for="user">No.Telp / HP</label>
                                                
                                                <input type="text" class="form-control"     
                                                        id="no_telp" 
                                                        placeholder="{{ isset(Auth::user()->no_telp) ? Auth::user()->no_telp : '08xxxxxxx' }}" name="no_telp">
                                            </div>
                                            <div class="form-group">
                                                <span>Choose file</span>
                                                <input type="file" class="form-control" placeholder="Foto" name="foto"> 
                                            </div>
                                            <br>
                                            <button type="submit" class="btn btn-primary" value="Edit Data User" >Save</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>	 
		</div>
    </div>
@endsection