@extends('_app.admin', ['isTipsActive' => true])

@section('content-header', 'Add Data Tips And Trick')

@section('content')

    <div class="col-lg-12">
	    <div class="box box-primary">
            <div class="box-body">
                @if( session('status') )
                    <div class="alert alert-{{ session('alert') }}">
                        {{ session('status') }}
                    </div>
                @endif
	            <form action="{{ route('admin.add.tips.proses') }}" method="POST" enctype="multipart/form-data">
                    @csrf
		            <div class="form-group">
		                <h4>Isi Artikel</h4>
                        <div class="form-group">
                                <h4>Judul</h4>
                                <input type="text" name="judul" class="form-control">
                        </div>   
                        <div class="form-group">
                            <h4>Gambar</h4>
                            <input type="file" class="form-control" name="img" >
                        </div>
                        <div class="form-group">
                            <h4>Jenis Materi</h4>
                            <select name="id_materi" class="form-control">
                                @foreach($data as $materi)
                                    <option value="{{ $materi->id }}">
                                        {{ $materi->nama }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
		                <textarea class="ckeditor" name="isi" id="editor1"></textarea>
		            </div>
                    <button type="submit" name="submit" class="btn btn-primary">
                        Tambah Tips
                    </button>
	            </form>
            </div>
        </div>
    </div>

@endsection