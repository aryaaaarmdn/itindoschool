@extends('_app.admin', ['isSubMateriActive' => true])

@section('content-header', 'Add Sub Materi')

@section('breadcrumb')
    <li class="active">
        Add Sub Materi
    </li>
@endsection

@section('content')
    <div class="col-lg-12">
	    <div class="box box-primary">
            <div class="box-body">
                @if( session('status') )
                    <div class="alert alert-{{ session('alert') }}">
                        {{ session('status') }}
                    </div>
                @endif
	            <form action="{{ route('admin.add.submateri.proses') }}" method="POST" enctype="multipart/form-data">
                    @csrf
	                <div class="form-group">
		                <h4>Nama Sub materi</h4>
		                <input type="text" class="form-control" required 
                                name="nama" placeholder="Nama Jenis materi...">
		            </div>
		            <div class="form-group">
			            <h4>Jenis Materi</h4>
			            <select name="id_materi" class="form-control" required>
                            <option value="0">-- Pilih Jenis Materi --</option>
                            @foreach($data as $materi)
                                <option value="{{ $materi->id }}">
                                    {{ $materi->nama }}
                                </option>
                            @endforeach
                        </select>
		            </div>
                    <div class="form-group">
                        <h4>Thumbnail Sub Materi</h4>
                        <input type="file" class="form-control" name="img" required>
                    </div>
		            <button type="submit" class="btn btn-primary">Tambah Sub Materi</button>
	            </form>
            </div>
        </div>
    </div>
@endsection