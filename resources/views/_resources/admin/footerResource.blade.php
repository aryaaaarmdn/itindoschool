    <script src="{{ asset('assets/admin-template/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <script src="{{ asset('assets/admin-template/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/admin-template/bower_components/fastclick/lib/fastclick.js') }}"></script>
    <script src="{{ asset('assets/admin-template/dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('assets/admin-template/dist/js/demo.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
        
        })
    </script>
    <script type="text/javascript">
        $(document).ready( function () {
            $('.table').DataTable();
        
        });
    </script>
    <script>
        tinymce.init({
            selector:'textarea',
            plugins:"image,media",
            theme:'modern'
        });
    </script>
    <script>
        $(document).ready(function () {
            $(".deleteMateriButton").click(function () {
                let id = $(this).data('id');
                let url = '{{ route("admin.delete.materi", ":id") }}';
                url = url.replace(':id', id);
                $('#formDelete').attr('action', url);
            });

            $(".deleteSubMateriButton").click(function () {
                let id = $(this).data('id');
                let url = '{{ route("admin.delete.submateri", ":id") }}';
                url = url.replace(':id', id);
                $('#formDelete').attr('action', url);
            });

            $(".deleteUserButton").click(function () {
                let id = $(this).data('id');
                console.log(id);
                let url = '{{ route("admin.delete.user", ":id") }}';
                url = url.replace(':id', id);
                $('#formDelete').attr('action', url);
            });

            $(".deleteDataMateriButton").click(function () {
                let id = $(this).data('id');
                let url = '{{ route("admin.delete.datamateri", ":id") }}';
                url = url.replace(':id', id);
                $('#formDelete').attr('action', url);
            });

            $(".deleteSubMateriSoalButton").click(function () {
                let id = $(this).data('id');
                let url = '{{ route("admin.submateri.soalujian.delete", ":id") }}';
                url = url.replace(':id', id);
                $('#formDelete').attr('action', url);
            });

            $(".deleteDataMateriSoalButton").click(function () {
                let id = $(this).data('id');
                let url = '{{ route("admin.data.materi.destroy", ":id") }}';
                url = url.replace(':id', id);
                $('#formDelete').attr('action', url);
            });

        });
    </script>