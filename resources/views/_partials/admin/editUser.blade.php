@extends('_app.admin', ['isUsersActive' => true])

@section('content-header', 'Edit Data Users')

@section('breadcrumb')
    <li class="active">
        Edit Data Users
    </li>
@endsection

@section('content')

    <div class="col-lg-12">
	    <div class="box box-primary">
            <div class="box-body">
                @if( session('status') )
                    <div class="alert alert-{{ session('alert') }}">
                        {{ session('status') }}
                    </div>
                @endif
	            <form action="{{ route('admin.edit.user.proses', $data->id) }}" method="POST">
                    @method('put')
                    @csrf
                    <div class="form-group">
                        <label for="">Nama</label>
                        <input type="text" name="nama" required value="{{ $data->nama }}"  placeholder="Ubah Nama..."  class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="text" name="email" required value="{{ $data->email }}"  placeholder="Ubah Email..."  class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="text" name="password" placeholder="Ubah Password..." class="form-control">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-md btn-primary">Edit Data User</button>
                    </div>
	            </form>
            </div>
        </div>
    </div>


@endsection