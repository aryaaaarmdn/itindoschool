<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataMaterial extends Model
{
    use HasFactory;

    protected $fillable = [
        'judul',
        'isi_materi',
        'jml_view',
        'img_thumb',
        'video_materi',
        'sub_material_id',
        'admin_id',
        'status_materi',
        'no_materi',
        'video_materi2'
    ];

    public function submaterial()
    {
        return $this->belongsTo(SubMaterial::class, 'sub_material_id');
    }

    public function questions()
    {
        return $this->hasMany(DataMaterialQuestion::class);
    }

    public function getCreatedAttribute()
    {
        return "{$this->created_at}";
    }
}
