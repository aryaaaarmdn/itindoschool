<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Homepage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::prefix('/')->group(function () {
    Route::get('/', [Homepage::class, 'index'])->name('homepage');

    Route::get('about', [Homepage::class, 'about'])->name('about');

    Route::get('forum', [Homepage::class, 'forum'])->name('forum');

    Route::get('materi/{materi}', [Homepage::class, 'materi'])
        ->middleware('auth:web')
        ->name('materi');
    
    Route::get('materi/{materi}/{detail_materi}/{index?}', [Homepage::class, 'detailMateri'])
        ->middleware('auth:web')
        ->name('detail.materi');
    
    Route::get('tips/{id_materi?}', [Homepage::class, 'Tips'])
        ->name('tips');
    
    Route::get('tips/baca/{id_tips}', [Homepage::class, 'BacaTips'])
        ->name('baca.tips');

    Route::get('profile', [UserController::class, 'profile'])
        ->middleware('auth:web')
        ->name('profile');

    Route::get('edit_profile', [UserController::class, 'editprofile'])
        ->middleware('auth')
        ->name('editProfile');

    Route::put('edit_profile', [UserController::class, 'updateProfile'])
        ->middleware('auth')
        ->name('updateProfile');
});

Route::prefix('admin')->group(function() {
    Route::get('/', [AdminController::class, 'index'])
        ->middleware('auth:admin')
        ->name('admin.dashboard');

    Route::get('/login', [AdminController::class, 'login'])
        ->middleware('guest:admin')
        ->name('admin.login');
    
    Route::post('/login', [AdminController::class, 'login_process'])
        ->middleware('guest:admin')
        ->name('admin.login.process');

    Route::post('/logout', [AdminController::class, 'logout'])
        ->middleware('auth:admin')
        ->name('admin.logout');
    
    Route::get('/materi', [AdminController::class, 'materi'])
        ->middleware('auth:admin')
        ->name('admin.materi');
    
    Route::get('/add_materi', [AdminController::class, 'addMateri'])
        ->middleware('auth:admin')
        ->name('admin.add.materi');
    
    Route::post('/add_materi', [AdminController::class, 'addMateriProses'])
        ->middleware('auth:admin')
        ->name('admin.add.materi.proses');
    
    Route::get('/edit_materi/{id}', [AdminController::class, 'editMateri'])
        ->middleware('auth:admin')
        ->name('admin.edit.materi');
    
    Route::put('/edit_materi/{id}', [AdminController::class, 'editMateriProses'])
        ->middleware('auth:admin')
        ->name('admin.edit.materi.proses');

    Route::delete('/delete_materi/{id}', [AdminController::class, 'deleteMateri'])
        ->middleware('auth:admin')
        ->name('admin.delete.materi');
    
    Route::get('/submateri', [AdminController::class, 'subMateri'])
        ->middleware('auth:admin')
        ->name('admin.submateri');

    Route::get('/add_submateri', [AdminController::class, 'addSubMateri'])
        ->middleware('auth:admin')
        ->name('admin.add.submateri');
    
    Route::post('/add_submateri', [AdminController::class, 'addSubMateriProses'])
        ->middleware('auth:admin')
        ->name('admin.add.submateri.proses');
    
    Route::get('/edit_submateri/{id}', [AdminController::class, 'editSubMateri'])
        ->middleware('auth:admin')
        ->name('admin.edit.submateri');
    
    Route::put('/edit_submateri/{id}', [AdminController::class, 'editSubMateriProses'])
        ->middleware('auth:admin')
        ->name('admin.edit.submateri.proses');

    Route::delete('/delete_submateri/{id}', [AdminController::class, 'deleteSubMateri'])
        ->middleware('auth:admin')
        ->name('admin.delete.submateri');
    
    Route::get('submateri/soal_ujian/{id}', [AdminController::class, 'SubmateriSoal'])
        ->middleware('auth:admin')
        ->name('admin.submateri.soalujian');
    
    Route::post('submateri/soal_ujian', [AdminController::class, 'AddSubmateriSoalProses'])
        ->middleware('auth:admin')
        ->name('admin.submateri.soalujian.proses');

    Route::get('submateri/soal_ujian/edit/{id}', [AdminController::class, 'SubmateriSoalEdit'])
        ->middleware('auth:admin')
        ->name('admin.submateri.soalujian.edit');
    
    Route::put('submateri/soal_ujian/edit/{id}', [AdminController::class, 'SubmateriSoalEditProses'])
        ->middleware('auth:admin')
        ->name('admin.submateri.soalujian.edit.proses');

    Route::delete('submateri/soal_ujian/{id}/delete', [AdminController::class, 'subMateriSoalDelete'])
        ->middleware('auth:admin')
        ->name('admin.submateri.soalujian.delete');

    Route::get('data-materi/soal/{id}', [AdminController::class, 'DataMateriSoal'])
        ->middleware('auth:admin')
        ->name('admin.data.materi.soal');
    
    Route::post('data-materi/soal', [AdminController::class, 'DataMateriSoalStore'])
        ->middleware('auth:admin')
        ->name('admin.data.materi.soal.store');
    
    Route::get('data-materi/soal/{id}/edit', [AdminController::class, 'DataMateriSoalEdit'])
        ->middleware('auth:admin')
        ->name('admin.data.materi.soal.edit');
    
    Route::put('data-materi/soal/{id}/edit', [AdminController::class, 'DataMateriSoalUpdate'])
        ->middleware('auth:admin')
        ->name('admin.data.materi.soal.update');
    
    Route::delete('data-materi/soal/{id}/delete', [AdminController::class, 'DataMateriSoalDelete'])
        ->middleware('auth:admin')
        ->name('admin.data.materi.destroy');
    
    
    Route::get('data-materi', [AdminController::class, 'dataMateri'])
        ->middleware('auth:admin')
        ->name('admin.dataMateri');
    
    Route::get('data-materi/add', [AdminController::class, 'addDataMateri'])
        ->middleware('auth:admin')
        ->name('admin.add.dataMateri');
    
    Route::post('data-materi/add', [AdminController::class, 'addDataMateriProses'])
        ->middleware('auth:admin')
        ->name('admin.add.dataMateri.proses');
    
    Route::delete('data-materi/delete/{id}', [AdminController::class, 'deleteDataMateri'])
        ->middleware('auth:admin')
        ->name('admin.delete.datamateri');
    
    Route::get('data-materi/edit/{id}', [AdminController::class, 'editDataMateri'])
        ->middleware('auth:admin')
        ->name('admin.edit.datamateri');

    Route::put('data-materi/edit/{id}', [AdminController::class, 'editDataMateriProses'])
        ->middleware('auth:admin')
        ->name('admin.edit.datamateri.proses');

    Route::get('/users', [AdminController::class, 'showUsers'])
        ->middleware('auth:admin')
        ->name('admin.users');
    
    Route::delete('/user/{id}', [AdminController::class, 'deleteUser'])
        ->middleware('auth:admin')
        ->name('admin.delete.user');

    Route::get('/user/edit/{id}', [AdminController::class, 'editUser'])
        ->middleware('auth:admin')
        ->name('admin.edit.user');

    Route::put('/user/edit/{id}', [AdminController::class, 'editUserProses'])
        ->middleware('auth:admin')
        ->name('admin.edit.user.proses');
    
    Route::get('tips', [AdminController::class, 'Tips'])
        ->middleware('auth:admin')
        ->name('admin.tips');
    
    Route::get('add-tips', [AdminController::class, 'addTips'])
        ->middleware('auth:admin')
        ->name('admin.add.tips');
    
    Route::post('add-tips', [AdminController::class, 'addTipsProses'])
        ->middleware('auth:admin')
        ->name('admin.add.tips.proses');

    Route::delete('tips/{id}/delete', [AdminController::class, 'tipsDelete'])
        ->middleware('auth:admin')
        ->name('admin.delete.tips');
    
});

Route::get('/dashboard', function () {
    return view('dashboard');
})
    // ->middleware(['auth'])
    ->name('dashboard');

require __DIR__.'/auth.php';
