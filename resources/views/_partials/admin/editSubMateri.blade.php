@extends('_app.admin', ['isSubMateriActive' => true])

@section('content-header', 'Edit Sub Materi')

@section('content')

    <div class="col-lg-12">
	    <div class="box box-primary">
            <div class="box-body">
	            <form action="{{ route('admin.edit.submateri.proses', $data->id) }}" method="POST" 
                    enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
		            <div class="form-group">
		                <h4>Nama Sub materi</h4>
		                <input type="text" required name="nama" 
                            class="form-control" value="{{ $data->nama }}">
		            </div>
		            <div class="form-group">
		                <h4>Jenis Materi</h4>
		                <select name="id_materi" class="form-control" required>
					        <option value="0" disabled>-- Pilih Jenis Materi --</option>
                            @foreach($materi as $materi)
                                <option value="{{ $materi->id }}" {{ $materi->id == $data->material_id ? 'selected' : '' }}>
                                    {{ $materi->nama }}
                                </option>
                            @endforeach
		                </select>
	                </div>
                    <div class="form-group">
                        <h4>Thumbnail Sub Materi</h4>
                        <input type="file" class="form-control" name="img">
                    </div>
		            <button type="submit" class="btn btn-primary">Edit Sub Materi</button>
	            </form>
            </div>
        </div>
    </div>

@endsection