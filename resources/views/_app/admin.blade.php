@include('_layouts.header')

    @if(Route::is('admin.login'))
        <div class="login-box">
            @yield('content')
        </div>
    @else 
        <div class="wrapper">
            <header class="main-header">
                <a href="index2.html" class="logo">
                    <span class="logo-mini"><b>I</b>T</span>
                    <span class="logo-lg"><b>IT</b>INDO</span>
                </a>
                <nav class="navbar navbar-static-top">
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown notifications-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell-o"></i>
                                    <span class="label label-warning">10</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have 10 notifications</li>
                                    <li>
                                        <ul class="menu">
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-users text-aqua"></i> 
                                                    5 new members joined today
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-warning text-yellow"></i> 
                                                    Very long description here that may not fit into the page and may cause design problems
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-users text-red"></i> 5 new members joined
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-user text-red"></i> You changed your username
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#">View all</a></li>
                                </ul>
                            </li>
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="" class="user-image" alt="User Image">
                                    <span class="hidden-xs"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="user-header">
                                        <img src="" class="img-circle" alt="User Image">
                                        <p>
                                            <small>Member since Nov. 2012</small>
                                        </p>
                                    </li>
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <form method="POST" action="{{ route('admin.logout') }}">
                                                @csrf
                                                <a class="btn btn-default btn-flat" 
                                                    href="{{ route('admin.logout') }}"
                                                    onclick="event.preventDefault();
                                                            this.closest('form').submit();">
                                                    Logout
                                                </a>
                                            </form>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <aside class="main-sidebar">
                <section class="sidebar">
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p></p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header">MAIN NAVIGATION</li>
                        <li class="{{ isset($isDashboardActive) ? 'active' : '' }}">
                            <a href="{{ route('admin.dashboard') }}">
                                <i class="fa fa-dashboard"></i><span>Dashboard</span>
                            </a>
                        </li>
                        <li class="{{ isset($isMateriActive) ? 'active' : '' }}">
                            <a href="{{ route('admin.materi') }}">
                                <i class="fa fa-th"></i><span>Data Jenis Materi</span>
                            </a>
                        </li>
                        <li class="{{ isset($isSubMateriActive) ? 'active' : '' }}">
                            <a href="{{ route('admin.submateri') }}">
                                <i class="fa fa-th"></i><span>Data Sub Materi</span>
                            </a>
                        </li>
                        <li class="{{ isset($isDataMateriActive) ? 'active' : '' }}">
                            <a href="{{ route('admin.dataMateri') }}">
                                <i class="fa fa-th"></i><span>Data Materi</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#">
                                <i class="fa fa-globe"></i><span>Data Event</span>
                            </a>
                        </li>
                        <li class="{{ isset($isUsersActive) ? 'active' : '' }}">
                            <a href="{{ route('admin.users') }}">
                                <i class="fa fa-users"></i><span>Data User</span>
                            </a>
                        </li>
                
                        <li class="{{ isset($isTipsActive) ? 'active' : '' }}">
                            <a href="{{ route('admin.tips') }}">
                                <i class="fa fa-users"></i><span>Tips And Trick</span>
                            </a>
                        </li>

                        <li class="">
                            <a href="#">
                                <i class="fa fa-users"></i><span>Forum</span>
                            </a>
                        </li>
                    </ul>
                </section>
            </aside>
            <div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        @yield('content-header', '')
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <a href="#">
                                <i class="fa fa-dashboard"></i> Dashboard 
                            </a>
                            @yield('breadcrumb', '')
                        </li>
                        <li class=""></li>
                    </ol>
                </section>
                <section class="content">
                    <div class="row">
                        @yield('content')
                    </div>
                </section>
            </div>
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 2.4.0
                </div>
                <strong>
                    Copyright &copy; 2019 
                    <a href="https://adminlte.io">IT INDO SCHOOL</a>.
                </strong> All rights reserved.
            </footer>
            @if( 
                    isset($materi->id) || isset($submateri->id) || isset($user->id) ||
                    isset($question->id)
                )
                <div id="deleteData" class="modal fade" role="dialog">
                    <!-- <form action="{{ route('admin.delete.materi', $materi->id ?? '') }}" 
                        method="post" id="formDeleteMateri"> -->
                    <form action="#" 
                        method="post" id="formDelete">
                        @method('delete')
                        @csrf
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Delete Data</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Apa Anda yakin ingin menghapus data ?</p>
                                </div>
                                <div class="modal-footer">
                                    <!-- <a class="btn btn-danger" id="deleteBtn">Yes</a> -->
                                    <button type="submit" class="btn btn-danger">Yes</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            @endif
        </div>
    @endif

@include('_layouts.footer')